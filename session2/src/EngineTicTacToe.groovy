class EngineTicTacToe {

    int N = 3
    int[][] field = new int[N][N]
    int X = 1
    int O = -1
    int player = 1

    boolean isGameOver() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (field[i][j] == 0) {
                    return false
                }
            }
        }
        return true
    }

    void setPlayer(player) {
        if (player == X) {
            this.player = O
        } else {
            this.player = X
        }
    }

    boolean turn(x, y) {
        --y
        --x
        if (field[x][y] == 0) {
            field[x][y] = player
            setPlayer()
            return true
        } else {
            return false
        }

    }

    char winner() {

    }
}
