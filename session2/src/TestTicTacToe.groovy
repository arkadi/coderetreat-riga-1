class TestTicTacToe extends GroovyTestCase {

    void testIsGameOver() {
        def t = new EngineTicTacToe()
        assert !t.isGameOver()
    }

    void testWinner() {
        def t = new EngineTicTacToe()
        t.turn(1, 1) // X
        t.turn(1, 2) // O
        t.turn(2, 1) // X
        t.turn(1, 3) // O
        t.turn(3, 1) // X
        assert t.isGameOver()
        assert t.winner() == 'X'
    }

    void testTurnValid() {
        def t = new EngineTicTacToe()
        assert t.turn(1, 1)
        assert !t.turn(1, 1)
    }

}
