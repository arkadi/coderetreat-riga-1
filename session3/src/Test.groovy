class Test extends GroovyTestCase {
    def a = 'a'
    def b = 'b'
    def c = 'c'

    void testIsEmptyBoardEmpty() {
        def board = [:]
        assert board.size() == 0
    }

    void testIsFullBoardFull() {
        def board = [:]
        board[['a', 'a']] = 'X'
        board[['a', 'b']] = 'O'
        board[['a', 'c']] = 'X'
        board[['b', 'a']] = 'O'
        board[['b', 'b']] = 'X'
        board[['b', 'c']] = 'O'
        board[['c', 'a']] = 'X'
        board[['c', 'b']] = 'O'
        board[['c', 'c']] = 'X'
        assert board.size() == 9
    }

    void testWinner() {
        def board = [:]
        board[['a', 'a']] = 'X'
        board[['a', 'b']] = 'X'
        board[['a', 'c']] = 'X'
        board[['c', 'b']] = 'O'
        assert 'X' == winner(board)

        def board2 = [:]
        board2[['a', 'a']] = 'X'
        board2[['b', 'a']] = 'X'
        board2[['c', 'a']] = 'X'
        board2[['c', 'b']] = 'O'
        assert 'X' == winner(board2)

        def board3 = [:]
        board3[['a', 'a']] = 'X'
        board3[['b', 'b']] = 'X'
        board3[['c', 'c']] = 'X'
        board3[['c', 'b']] = 'O'
        assert 'X' == winner(board3)

        def board4 = [:]
        board4[['c', 'a']] = 'X'
        board4[['b', 'b']] = 'X'
        board4[['a', 'c']] = 'X'
        board4[['c', 'b']] = 'O'
        assert 'X' == winner(board4)
    }

    def winner(Map board) {
        winner(board, 'X') || winner(board, 'O') || board.size() == 9 ? "Draw" : false
    }

    def winner(Map board, String who) {
        def cells = [a, b, c]
        cells.any { x ->
            cells.every { y ->
                board[[x, y]] == who || board[[y, x]] == who
            }
        } ||
        cells.every { c ->
            board[[c, c]] == who
        } ||
        [[c, a], [b, b], [a, c]].every { coords ->
            board[coords] == who
        }
    }
}
