class TicTacToe extends GroovyTestCase {
    def realBoard = new TicTacToeBoard()
    def who = 'X'
    def n = 3

    def board() {
        realBoard.board
    }

    def turn(x, y, z) {
        if (!realBoard.turn([x, y, z], who))
            return false
        who = who == 'X' ? 'O' : 'X'
        true
    }

    def winner() {
        winner('X') ? 'X' : winner('O') ? 'O' : draw() ?: false

    }

    def winner(who) {
        def n = 3
        (0..<n).any { x ->
            winner(n, who, x)
        }
    }

    def winner(n, who, x) {
        (0..<n).any { y ->
            winner(n, who, x, y) || winner(n, who, y, x)
        }
    }

    def winner(n, who, x, y) {
        (0..<n).any { z ->
            board()[[x, y, z]] == who || board()[[x, z, y]] == who
        }
    }

    def draw() {
        board().size() == n * n * n
    }
}
