class TicTacToeTest extends GroovyTestCase {

    void testSimpleTurn() {
        def t = new TicTacToe()
        assert t.turn(1, 1, 1)
        assert t.board() == [[1, 1, 1]: 'X']
        assert !t.turn(1, 1, 1)
        assert t.board() == [[1, 1, 1]: 'X']
    }

    void testWinner() {
        def t = new TicTacToe()
        t.turn(1, 1, 1)
        t.turn(2, 1, 1)
        t.turn(1, 2, 1)
        t.turn(2, 2, 1)
        t.turn(1, 3, 1)
        assert 'X' == t.winner()
    }
}
