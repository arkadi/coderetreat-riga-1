class TicTacToeBoard {
    def board = [:]

    def turn(coords, who) {
        if (board.containsKey(coords))
            return false
        board[coords] = who
        true
    }
}
