class TicTacToe {

    def N = 3
    def X = 'X'
    def Y = 'Y'
    def battlefield = [:]
    def lastMark = 'Y'

    def winner(who) {
        false
    }

    def gameEnded() {
        winner(X) || winner(Y) || draw() || false
    }


    private def badCoords(List coords) {
        coords.any { coord ->
            coord <= 0 || coord > N
        }
    }

    def place(x, y) {
        def coords = [x, y]
        if (battlefield.containsKey(coords) || badCoords(coords))
            return false
        battlefield[coords] = lastMark = lastMark == X ? Y : X
        true
    }
}
