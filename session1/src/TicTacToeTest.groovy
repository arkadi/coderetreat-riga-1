import org.junit.Test

@org.junit.runner.RunWith(org.junit.runners.JUnit4.class)
class TicTacToeTest extends GroovyTestCase {

    TicTacToe t

    @org.junit.Before
    void init() {
        t = new TicTacToe()
    }

    @Test
    void testGameNotEnded() {
        assert t.gameEnded() == false
    }

    @Test
    void testDraw() {
        t.N.times { x ->
            t.N.times { y ->
                t.place(x, y)
            }
        }
        assert t.gameEnded() == "draw"
    }

    @Test
    void testPlaceMark() {
        t.place(2, 1)
        assert t.battlefield == [[2, 1]: t.X]
    }

    @Test
    void testPlaceInvalidMark() {
        assert t.place(2, 1)
        assert !t.place(2, 1)
        assert !t.place(20, 1)
        assert t.place(1, 2)
    }

}
